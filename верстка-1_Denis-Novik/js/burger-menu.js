function clickMenu() {
    let burgerMenu =  document.getElementById('open-menu-mobile');
    
    if(burgerMenu.classList.contains("open-menu-mobile-show")) {
        burgerMenu.classList.remove("open-menu-mobile-show");
        burgerMenu.classList.add("open-menu-mobile-hide");
    } else {
        burgerMenu.classList.remove("open-menu-mobile-hide");
        burgerMenu.classList.add("open-menu-mobile-show");
    }
}